# Sessiones  
Se utilizan para guardar información.  
Protocolo HTTP, información no persistente.  
Session: array con información, solo accesible desde el navegador en el que las he abierto.  

## Persistencia de las sesiones aun habiendo cerrado el navegador  
Se usan cookies (persistentes), 
 
   
## Sesiones en Laravel  
Con request.  
  
### Tipos de sesiones en Laravel  
* archivo (en el servidor)
* cookies (en el cliente)
* DB
* memcached /redis => cache (fast access)
* array (not peristent)
  
Por defecto se usa el file.  
  
#### Como se utiliza?  
  
```
// write
$request->session()->put('key','value');

// read
$value = $request->session()->get('key');

// read with null/not set failsafe
$value = $request->session()->get('key', 'default);
$value = $request->session()->get('key' function() {
return default;
});

```
  
### Flash (micro sesion)  
  
Información persistente en formularios.  
  
  
### Laravel session config  
config => session.php
  

