# Exceptions  
  
## PDO  
En laravell equivale a Illuminate.  
En laravel pueden salir **errores** tanto de tipo PDO como de tip Illuminate.  
  
**ERROR** en vez de Exception. Lo ideal es poderlos tratar con Request, pero tambien se pueden tratar con **custom exceptions**.  
  
### Ejemplo errores y excepciones.  
* (...)->save => QueryException => catch (\Illuminate\Database\QueryException $exception)
* custom => catch(UploadFileException $exception $exception) {
}  
  
### Custom exceptions  
#### Services  
Son clases que yo creo para poder utilizar a lo largo de todo mi programa (sacarlo del programa).  
Ejemplo, **subir un archivo**, **limite de tamaño de imagen**, etc.  
  
#### En el controller  
function (...) save(Request, $request , Service here)  
  
En services =>  
```
class UploadFileService {
	function uploadFile($file) {
		if ($file == null) {
			throw new }
	}
}
``` 
En Exceptions (crear nueva exception en artisan)  
```
class UploadFIleException extends Exception {
	public function customMessage() {
		return 'No has subido ninguna foto';
	}
}
```
  
### Provider  
Hay que registrarlo y cargarlo en Provider.  
Registrarlo en **config/app.php  
  
### Service Provider  
Provider registra service. Puede ser utilizado en cualquier metodo, etc. No solo Controller.  
  
## Errors/Exceptions I want to make  
* upload file exception (from file is null)
* user not found exception (not factible only available in login screen)  
* send mail
  
### Atención!  
Excepcions, Service/Provider 
