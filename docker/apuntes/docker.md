# Docker fedora  
Teneis que ejecutar  script adjunto (cuidado que se reinicia el PC).  
  
 sh [archivo.sh]  
  
Al arrancar vuestro PC siempre debereis de poner estas lineas en el terminal:  
  
```
sudo mkdir /sys/fs/cgroup/systemd
sudo mount -t cgroup -o none,name=systemd cgroup /sys/fs/cgroup/systemd
```
  
He pedido que inserte en el script de inicio de sesión, pero de momento tendreis que ponerlas cada vez que arrancais el equipo.  
  
Ya ya podreis ejecutar mediante sudo:  
```
sudo docker-compose up -d
```
  
   
Si trabajais con diferentes proyectos es posible que en algun momento os de error de que esas maquinas ya existen. La manera mas facil de solucionar esto es eliminar los dockers viejos con :  
```
docker system prune -a
```
  
# Pt2  
Aqui os dejo el script actualizado para el error de Debian host unreachable. Simplemente le añado al Firewall de fedora para que acepte conexiones del docker.  
  
Si aun asi no va, desactivad el firewall  
```
sudo systemctl stop firewalld
sudo systemctl disable firewalld
```
https://github.com/dantriano/fedora32_docker
