version: '3'
services:
  #Node Service
  app:
    image: node:15-alpine3.10
    container_name: app_${DOCKER_NAME}
    restart: unless-stopped
    entrypoint: sh /scripts/init.sh
    tty: true
    working_dir: /app
    volumes:
      - ${PROJECT_PATH}:/app
      - ./scripts/init.sh:/scripts/init.sh
    ports:
      - ${SERVER_PORT}:${SERVER_PORT}
    networks:
      - app-network

  # Postgres
  postgres:
    image: postgres
    container_name: postgres_${DOCKER_NAME}
    restart: unless-stopped
    environment: 
      POSTGRES_USER: ${POSTGRES_USER}
      POSTGRES_PASSWORD: ${POSTGRES_PASSWORD}
      POSTGRES_DB: ${POSTGRES_DB}
    ports: 
      - ${POSTGRES_PORT}:${POSTGRES_PORT}
    volumes:
      - ${DATA_PATH}:/data/db
    networks:
      - app-network
#Docker Networks
networks:
  app-network:
    driver: bridge
#Volumes
volumes:
  dbdata:
    driver: local
  


