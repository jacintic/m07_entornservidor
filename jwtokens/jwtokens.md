# Uthentication in node  
  
Node => Vue  
  
Node => API =>  Vue 
Vue => API => Node  
  
## Token  
JWT => Json Web Token  
```
{
id: 777,
role: 'admin'
}
// JWT codifica esa información en un string
// ex
7854723823832
// puede añadir un expire date
```
Node envia el token, Vue lo guarda para enviarlo en las siguientes peticiones.  
Vue en el header de la petición http añade el token codificado.  
  
### JWT.verify  
Coje el token (213921831) y verifica si esta correcto y no ha caducado.  
Una vez el token ha sido verificado entonces se autroiza la operación.  
Para esto se utiliza un midleware. 
```
res.status = 500;
// esto aborta la transición a siguientes midlewares  
```
  
## bcrypt  
String => string encriptado  
Esto es para almacenar pw en la contraseña.  
  
## https  
Es el canal por el cual hemos de enviar el password.  

