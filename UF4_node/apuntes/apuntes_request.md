# Request  
```  
request([url,get],(err,res)) {
	console.log("Hola");
}  
```  
  
# async  
  
Cuando algunas peticiones/funciones envian peticiones a recursos externos (por ejemplo una api) que tardaran en responder. Por eso se introducen las pormises.  
  
```
return new Promis() {
	request([url,get],(err,res)) {
		resolve(res);
	}  
}


//

callPromise().then() {
	render(new res);
	console.log("fin");
}
```  
Con esto ya funciona.  
  
  

## async await  
  
```
return new Promis() {
	request([url,get],(err,res)) {
		resolve(res);
	}  
}


//
async function Api() {

	await callPromise().then() {
		render(chat, res);
		console.log("fin");
	}
}

// simulacion de carga
    ____chat_________
   |                |
----                |---
   |___incidenicas__|
```
Ahora el codigo esperara a la ejecución del codigo para continuar con el resto del codigo.  
  
  
### Mas ejemplos  
```
return new Promis() {
	request([url,get],(err,res)) {
		resolve(res);
	}  
}


//

route()async() {
	callPromise(chat).then() {
		render(new res);
		console.log("fin");
	}
}


call Promise(incidencias).then() {
	rener(incidencias,res);
}

// simulacion de carga
    ____chat____incidenicas
   |
----
```  
  
## POST  
Body parser.  
npm install body-parser  
var express = require('express');
var body_parser = require('body-parser');
