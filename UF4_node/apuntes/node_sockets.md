# Sockets  
  
Funciona mediante puertos de conexión TCP/IP, reserva un puerto. Es menos seguro, se codifican los paquetes. Es mas costoso para el servidor. 
  
Va por TCP/IP.  
  
Con sockets el servidor esta escuchando constantemente, y por lo tanto se requieren mas recursos. Hay que hacer un balance.  
  
  
## Sockets en Node  
  
Es un servicio mas. Cada vez que establecemos un vinculo entre cliente servidor se establece un canal.  
  
```

			   ________cli1
			  | 
Server-sockets -----------|
			  |________cli2


```  
  
Varios canales van al mismo puerto en el server de sockets.  
  
Dividir puertos. Pagina y socket. Puede ser util para por ejemplo cancelar el servicio sockets por una vulnerabilidad. Por lo tanto es conveniente tener el puerto de http (web) y sockets separados por si aca.  

### Socket.io  
  
Deve estar instalado en:  
* Server
* Cliente
  
Comunicación solo cliente - servidor.  
  
En el caso de un chat el server hace de centralita entre los usuarios.  
  
#### Metodos de sockets  
* on => escucha (por ejemplo el canal chat) socket.on("chat",(data))
* emit => enviar => socket.emit("m7",{data: 'Hola'}
* broadcast =>   enviar a todos
  
	* broadcast + on => 
```
socket.on("chat", (data) {
	socket.broadcast(data);
})
// esto enviaria a todos menos al que lo ha escrito.
```

Por ejemplo, `broadcast`  

#### En server  
`npm install sockets.io`  
  
#### En cliente  
`<src="sockets.js">`  
  

