<?php
if (!isset($_POST['players']) || !isset($_POST['matches']) ) {
    echo "Error: one or both POST parameter not found.";
    exit;
}
$myPlayers = $_POST['players'];
$myMatches = $_POST['matches'];

$i = 0;
$j = 0;
?>
<html>
    <body>
        <head>
            <meta charset="utf-8">
            <title></title>
            <link href="css/styles.css" rel="stylesheet" />
          </head>
          <body>
<h3>Players:</h3><br>
<form method="post" action="printTable.php">
<?php



while ($i < $myPlayers) {
    echo "Player " . ($i + 1) . "'s name: <input type='text' name='league[".$i."][name]'>";
    $j = 0;
    while ($j < $myMatches) {
        echo "Match " . ($j + 1). "'s goals: <input type='text' name='league[" . $i ."][goals][" . $j ."]'>";
        $j++;
    }
    $i++;
    echo "<br>";
}
?>

<input type="hidden" name="numplayers" value="<?php echo $myPlayers ?>">
<input type="hidden" name="nummatches" value="<?php echo $myMatches ?>">
<input type="submit" value="Send">
</form>
</body>
</html>