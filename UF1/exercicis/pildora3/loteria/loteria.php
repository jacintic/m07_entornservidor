<?php
$NUM_TO_SEARCH = 61;
$loteria = [61,32,43,61];
$index = 0;
foreach($loteria as $num) {
    if ( $num == $NUM_TO_SEARCH) {
        $index++;
    }
}
if ($index > 0) {
    echo $NUM_TO_SEARCH . " has been found ". $index . " times.";
    exit;
}
echo $NUM_TO_SEARCH . " hasn't been found.";
?>