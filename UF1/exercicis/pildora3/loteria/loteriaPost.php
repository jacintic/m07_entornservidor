<?php
if (!isset($_POST['num']) ) {
    echo "Error: POST parameter not found.";
    exit;
}
$myNum = $_POST['num'];
$loteria = [61,32,43,61];
$index = 0;
foreach($loteria as $num) {
    if ( $num == $myNum) {
        $index++;
    }
}
if ($index > 0) {
    echo $myNum . " has been found ". $index . " times.";
    exit;
}
echo $myNum . " hasn't been found.";
?>