<?php
ini_set('display_errors', 1);

ini_set('display_startup_errors', 1);

error_reporting(E_ALL);
if ( ! isset($_POST['user']) || ! isset($_POST['pass']) ) {
    echo "Error: failed to load POST parameters";
    exit;
}
$arrData = [
    "user1" => "pass1",
    "user2" => "pass2",
    "user3" => "pass3",
];
$myUser = $_POST['user'];
$myPass = $_POST['pass'];
if ($arrData[$myUser] == $myPass ) {
    echo "Hello " . $myUser . " you've logged in sucessfully";
    exit;
}
echo "Error: incorrect user or password.";
?>