# Pildora 1 exercicis.  
  
## 1. Investiga las diferentes opciones de servidores configurados para proporcionar servicio PHP.  
  
* LAMP - (Linux Apache MySQL PHP)
* XAMP - (X=> cross OS, Windows Mac or Linux) Apache, MySQL, PHP
* WAMP - (WindowsApache MySQL PHP)
* Custom install - Install Apache or Nginx, PHP and a database of choice, for example PostgreSQL (this is what I have at home, with Apache in Fedora 32).

## 2. Crea una pagina en la que se almacenan dos numeros en variables y muestre en una tabla los valores que toman las variables y cada uno de los resultados de todas las operaciones aritmeticas.   
  
=> exerciciAB.php  
  
```
<?php
$A = 4;
$B = 3;
echo "A = " . $A . "\n";
echo "B = " . $B . "\n";
echo "A+B = ". ($A + $B) . "\n";
echo "A-B = ". ($A - $B) . "\n";
echo "A*B = ". ($A * $B) . "\n";
echo "A/B = ". ($A / $B) . "\n";
echo "A^B = ". pow($A,$B) . "\n";
?>
```

# Pildora2: Syntaxis.  
  
## 1. Realiza todos los cuestionarios de la Pildora 2  
  
* get.php
* form_post.html
