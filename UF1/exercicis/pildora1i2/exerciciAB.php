<html>
<body>
<head>
<style>
* {
  box-sizing: border-box;
  padding: 0;
  margin: 0;
}

html, body {
  height: 100%;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
}

table, td {
  border-collapse: collapse;
  border: 1px solid black;
  text-align: center;
}
table {
  width: 75%;
}
</style>
</head>
<?php
$A = 4;
$B = 3;
/*
echo "A = " . $A . "\n";
echo "B = " . $B . "\n";
echo "A+B = ". ($A + $B) . "\n";
echo "A-B = ". ($A - $B) . "\n";
echo "A*B = ". ($A * $B) . "\n";
echo "A/B = ". ($A / $B) . "\n";
echo "A^B = ". pow($A,$B) . "\n";
 */
?>
<table>
<tr>
	<td>Operación</td><td>Valor</td>
</tr>
<tr>
<td>A</td><td><?php echo $A ?></td>
</tr>
<tr>
<td>B</td><td><?php echo $B ?></td>
</tr>
<tr>
<td>A + B</td><td><?php echo ($A + B) ?></td>
</tr>
<tr>
<td>A - B</td><td><?php echo($A - $B) ?></td>
</tr>
<tr>
<td>A * B</td><td><?php echo ($A * $B) ?></td>
</tr>
<tr>
<td>A / B</td><td><?php echo ($A / $B) ?></td>
</tr>
<tr>
<td>A ^ B</td><td><?php echo pow($A,$B)?></td>
</tr>
</table>
