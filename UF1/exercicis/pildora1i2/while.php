<?php
echo "To interact with this program, pass an array of numbers through GET like so \n";
echo "if.php?num=3&num=4&num=5\n";
if (! isset($_GET['num']) ) {
	echo "There was an error while trying to retrieve the parameters.";
	exit;
}
$arrObject = new ArrayObject($_GET['num']);

$arrayIterator = $arrObject->getIterator();

echo "List of numbers:\n";
while ($arrayIterator->valid() ) {
	echo $arrayIterator->current()."\n";
	$arrayIterator->next();
}
## esto no funcionara por que GET es un key value list y no un array
?>
