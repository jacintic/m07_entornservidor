<?php
echo "To interact with this program, pass the number to check for odd/even through get like so: \n";
echo "if.php?num=3\n";
if (! isset($_GET['num']) ) {
	echo "Ha habido un error al pasar el parametro num";
	exit;
}
$num = $_GET['num'];
$numIs = 'odd';
if ($num % 2 != 0) {
	$numIs = 'Even';
}
echo "Your number is ".$numIs;
?>
