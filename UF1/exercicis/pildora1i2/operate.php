<?php
if (! isset($_POST['numA']) || ! isset($_POST['numB']) || ! isset($_POST['operation']) ) {
	echo "Error, incomplete parameters, please fill in the whole form, first number, second number and operation";
	exit;
}
$numA = $_POST["numA"];
$numB = $_POST['numB'];
$operation = $_POST['operation'];
echo "Number A = " . $numA . "<br>";
echo "Number B = " . $numB . "<br>";
echo "Operation = " . $operation . "<br>";
echo "Operation is ";
switch ($operation) {
    case "sum":
        echo "sumatory " . $numA ." + " . $numB . " = " . ($numA + $numB);
        break;
    case "sut":
        echo "subtraction " . $numA ." - " . $numB . " = " . ($numA - $numB);
        break;
    case "mult":
        echo "multiplication " . $numA ." * " . $numB . " = " . ($numA * $numB);
	break; 
   case "div":
        echo "division " . $numA ." / " . $numB . " = " . ($numA / $numB);
	break;
  case "exp":
        echo "power " . $numA ." ^ " . $numB . " = " . pow($numA,$numB);
	break;
}
?>
