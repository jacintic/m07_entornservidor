
<!--
     8. Confeccionar una clase Empleado, definir como atributos su 
     nombre y sueldo. Definir un método inicializar que lleguen como 
     dato el nombre y sueldo. Plantear un segundo método que imprima 
     el nombre y un mensaje si debe o no pagar impuestos (si el sueldo 
     supera a 3000 paga impuestos)


-->
<?php
class Employee {
    private $name;
    private $salary;
    public function inicializar($name,$salary)
    {
      $this->name=$name;
      $this->salary=$salary;
    }
    public function print() {
        $payTax = false;
        if ($this->salary > 3000) {
            $payTax = true;
        }
        echo $this->name . ($payTax ? ' has to pay taxes.' : " doesn't have to pay taxes.") . "<br>";
    }
  }
   
  $John=new Employee();
  $John->inicializar('John',3000);
  $John->print();
  $Pol=new Employee();
  $Pol->inicializar('Pol',3001);
  $Pol->print();
?>