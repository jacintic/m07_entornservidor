
<!--
 14.Codificar la clase CabeceraDePagina que nos muestre un título alineado 
 con un determinado color de fuente y fondo. Definir en el constructor 
 parámetros predeterminados para los colores de fuente, fondo y el alineado 
 del título.

//establece caracteristicas del menu
__construct()
//Muestra el menu
graficar()
-->
<!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" href="css/title2.css">
      <title>Title</title>
  </head>
  <body>
<?php
class CabeceraDePagina {
  private $title;
  private $position;
  private $fColor;
  private $font;
  private $bgColor;

  public function __construct($title,$position,$fColor,$font,$bgColor) {
    $this->title = $title;
    $this->position = $position;
    $this->fColor = $fColor;
    $this->font = $font;
    $this->bgColor = $bgColor;
  }
  public function renderTitle() {
    echo "<h1 class='". $this->position . " " . $this->bgColor . " " . $this->font . " " . $this->fColor . "'>" . $this->title . "</h1>";
  }
 
  
}
$printTitle=new CabeceraDePagina("Hi there!","center","red","times","yellow");
$printTitle->renderTitle();
?> 

</body>
</html>