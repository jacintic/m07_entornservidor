
<!--
 13. Confeccionar una clase Menu. Permitir añadir la cantidad de opciones que necesitemos. 
 Mostrar el menú en forma horizontal o vertical, pasar a este método como parámetro el texto 
 “horizontal” o “vertical”. El método mostrar debe llamar alguno de los dos métodos privados 
 mostrarHorizontal() o mostrarVertical().
-->
<!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" href="css/menu.css">
      <title>Title</title>
  </head>
  <body>
<?php
class Menu {
  private $enlaces=[];
  private $titulos=[];
  public function cargarOpcion($enlaces,$titulos)
  {
    $this->enlaces[]=$enlaces;
    $this->titulos[]=$titulos;
  }
  private function mostrarVertical()
  {
    for($f=0;$f<count($this->enlaces);$f++)
    {
      echo '<li><a href="'.$this->enlaces[$f].'">'.$this->titulos[$f].'</a></li>';
    }
  }
  private function mostrarHorizontal()
  {
    for($f=0;$f<count($this->enlaces);$f++)
    {
      echo '<a class="menu" href="'.$this->enlaces[$f].'">'.$this->titulos[$f].'</a>';
    }
  }
 
  public function mostrar($orientacion)
  {
    if ($orientacion == "horizontal") {
        $this->mostrarHorizontal();
    } else if ($orientacion == "vertical") {
        $this->mostrarVertical();
    }
  }
}
 
$menu1=new Menu();
$menu1->cargarOpcion('http://www.lanacion.com.ar','La Nación');
$menu1->cargarOpcion('http://www.clarin.com.ar','El Clarín');
$menu1->cargarOpcion('http://www.lavoz.com.ar','La Voz del Interior');
$menu1->mostrar("horizontal");
echo '<br>';
$menu2=new Menu();
$menu2->cargarOpcion('http://www.google.com','Google');
$menu2->cargarOpcion('http://www.yahoo.com','Yhahoo');
$menu2->cargarOpcion('http://www.msn.com','MSN');
$menu2->mostrar("vertical");
?> 
</body>
</html>