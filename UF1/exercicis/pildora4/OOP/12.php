
<!--
 12. Confeccionar una clase Tabla que permita indicarle en el constructor 
 la cantidad de filas y columnas. Definir otro metodo que podamos cargar 
 un dato en una determinada fila y columna además de definir su color de 
 fuente y fondo. Finalmente debe mostrar los datos en una tabla HTML
-->
<?php
class Tabla {
    public $rows;
    public $cols;
    private $text;
    public $row;
    public $col;
    private $bgColor;
    private $font;

    public function __construct($rows,$cols) {
        $this->rows = $rows;
        $this->cols = $cols;
    }
    public function setTabEl ($row,$col,$text,$bgColor,$font) {
        $this->row = $row;
        $this->col = $col;
        $this->text = $text;
        $this->bgColor = $bgColor;
        $this->font = $font;
    }
    public function mostrar() {
        echo "<td class='" . $this->bgColor . " " . $this->font . "'>" . $this->text . "</td>";
    }
  }
   
  $tabEl = new Tabla(3,3);
  $tabEl->setTabEl(1,1,"Hello world!","red","times");
  ?>
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" href="css/h1style.css">
      <title>Title</title>
  </head>
  <body>
        <table border="1">
        <?php
        for ($i = 0; $i < $tabEl->rows; $i++) {
            echo "<tr>";
            for ($j = 0; $j < $tabEl->cols; $j++) {
                if ($i == $tabEl->row && $j == $tabEl->col) {
                    echo $tabEl->mostrar();
                } else {
                    echo "<td>0</td>";
                }
            }
            echo "</tr>";
        }
        ?>
        </table>
  </body>
  </html>

