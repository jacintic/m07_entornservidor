
<!--

9. Implementar una clase que muestre una lista de hipervínculos en forma horizontal 
(básicamente un menú de opciones)

    En este caso no hace falta formulario

//Metodo que añade un nuevo enlace al menu
cargarOpcion()
//Metodo que visualiza el menu en una pagina HTML
mostrar()


-->
<?php
class Links {
    private $link = [];

    public function cargarOpcion($link) {
        $this->link[] = $link;
    }
    public function mostrar() {
        foreach ($this->link as $el) {
            echo "<menuitem style='margin-right:15px;'><a href='" . $el . "'>" . $el . "</a></menuitem>";

        }
    }
  }
   
  $menu1=new Links();
  $menu1->cargarOpcion("wikipedia.org");
  $menu1->cargarOpcion("stackoverflow.com");
  $menu1->cargarOpcion("google.com");
  ?>
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Menu</title>
  </head>
  <body>
      <header>
        <menu> <?php $menu1->mostrar() ?></menu>
      </header>
  </body>
  </html>
