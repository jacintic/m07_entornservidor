
<!--

 10. Confeccionar una clase CabeceraPagina que permita mostrar un título, 
 indicarle si queremos que aparezca centrado, a derecha o izquierda, 
 además permitir definir el color de fondo y de la fuente. Todo esto son 
 propiedades del objecto Titulo

//Establece los valores de los atributos
inicializar()
//Metodo que visualiza el menu en una pagina HTML
mostrar()


-->
<?php
class CabeceraPagina {
    private $title;
    private $position;
    private $bgColor;
    private $font;

    public function inicializar($title,$position,$bgColor,$font) {
        $this->title = $title;
        $this->position = $position;
        $this->bgColor = $bgColor;
        $this->font = $font;
    }
    public function mostrar() {
        echo  $this->position . " " . $this->bgColor . " " . $this->font . "'>" . $this->title;
    }
  }
   
  $menu1=new CabeceraPagina();
  $menu1->inicializar("Welcome!","center","red","times");
  ?>
  <!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" href="css/h1style.css">
      <title>Title</title>
  </head>
  <body>
      <header>
        <h1 class='<?php $menu1->mostrar() ?></h1>
      </header>
  </body>
  </html>

