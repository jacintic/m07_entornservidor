
<!--
3. Una función que reciba como parámetros el valor del radio de la base 
y la altura de un cilindro y devuelva el volumen del cilindro, teniendo 
en cuenta que el volumen de un cilindro se calcula como 
Volumen = númeroPi * radio * radio * Altura siendo númeroPi = 3.1416 aproximadamente.
-->
<?php
function cylVol($rad,$height) {
    return (pi() * pow($rad,2) * $height);
}
$tmp = cylVol(3,2);
echo $tmp;
?>