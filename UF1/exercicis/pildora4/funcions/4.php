
<!--
 4. Crear un formulario en el que se introduce el precio de una 
 compra y realizar el programa que calcula mediante una función 
 la siguientes reglas Un comercio realiza descuentos con la 
 siguiente regla:

  Si la compra no alcanza los $100, no se realiza ningún descuento.
  Si la compra está entre $100 y $499,99, se descuenta un 10%.
  Si la compra supera los $500, se descuenta un 15%.
-->
<?php
$groceries = $_POST['groceries'];

function discountOut($groceries) {
    $result = "$ No discount.";
    if ($groceries >= 100 && $groceries <= 499.99) {
        $groceries -= $groceries * 0.1;
        $result = "$ 10% discount.";
    } else if ($groceries >= 500) {
        $groceries -= $groceries * 0.15;
        $result = "$ 15% discount.";
    }
    return $groceries . "" .$result;
}
$tmp = discountOut($groceries);
echo $tmp;
?>