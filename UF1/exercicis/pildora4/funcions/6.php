
<!--
 6. Realiza una función llamada intermedio(a, b) que a partir de 
 dos números, devuelva su punto intermedio. Cuando lo tengas 
 comprueba el punto intermedio entre -12 y 24
-->
<?php

function intermedio($a,$b) {
    return (($a + $b)/2);
}
$tmp = intermedio(-12,24);
echo $tmp ."<br>";
$tmp2 = intermedio(4,8);
echo $tmp2 ."<br>";
$tmp3 = intermedio(1,9);
echo $tmp3;
?>