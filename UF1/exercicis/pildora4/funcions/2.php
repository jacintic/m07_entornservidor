
<!--
2. Una función que reciba cinco números enteros como parámetros y devuelva el resultado 
de sumar los cinco números (tipo función, hay un valor devuelto). Asigna el resultado 
de una invocación a la función con los números 2, 5, 1, 8, 10 a una variable de nombre 
$tmp y muestra por pantalla el valor de la variable.
-->
<?php
function sum5Nums($num1,$num2,$num3,$num4,$num5) {
    return ($num1 + $num2 + $num3 + $num4 + $num5);
}
$tmp = sum5Nums(2,5,1,8,10);
echo $tmp;
?>