
<!--
 5. Realiza una función llamada relacion(a, b) que a partir de dos números cumpla lo siguiente:

  Si el primer número es mayor que el segundo, debe devolver 1.
  Si el primer número es menor que el segundo, debe devolver -1.
  Si ambos números son iguales, debe devolver un 0.
-->
<?php

function relacion($a,$b) {
    if ($a > $b) {
        return 1;
    } else if ($b > $a) {
        return -1;
    }
    return 0;
}
$tmp = relacion(2,1);
echo $tmp ."<br>";
$tmp2 = relacion(0,1);
echo $tmp2 ."<br>";
$tmp3 = relacion(0,0);
echo $tmp3;
?>