
<!--
1. Una función que reciba cinco números enteros como parámetros y muestre por pantalla el 
resultado de sumar los cinco números (tipo procedimiento, no hay valor devuelto). 
-->
<?php
function sum5Nums($num1,$num2,$num3,$num4,$num5) {
    return ($num1 + $num2 + $num3 + $num4 + $num5);
}
echo sum5Nums(1,2,3,4,5);
?>