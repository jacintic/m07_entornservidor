
<!--
     7. Realiza una función separar(lista) que tome una lista de 
     números enteros y devuelva (no imprimir) dos listas ordenadas. 
     La primera con los números pares y la segunda con los números 
     impares.

numeros = [-12, 84, 13, 20, -33, 101, 9]

Resultado:
[-12, 20, 84]
[-33, 9, 13, 101]
-->
<?php

function separar($lista) {
    sort($lista);
    return $lista;
}
$lista = [-12, 84, 13, 20, -33, 101, 9];
$tmp = separar($lista);
$even = [];
$odd = [];
foreach($tmp as $el) {
    if ($el % 2 == 0) {
        $even[] = $el;
    } else {
        $odd[] = $el;
    }
}
var_dump($even);
var_dump($odd);
?>