<?php
/*
1. Crea un formulario que puedas subir un archivo. En la 
pagina de destino ha de verse los atributos del archivo: 
nombre, tamaño, ubicación temporal, extensión… 
*/
// Check if the form was submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
    // Check if file was uploaded without errors
    if(isset($_FILES["photo"]) && $_FILES["photo"]["error"] == 0){
        $allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "gif" => "image/gif", "png" => "image/png");
        $filename = $_FILES["photo"]["name"];
        $filetype = $_FILES["photo"]["type"];
        $filesize = $_FILES["photo"]["size"];
    
        // Verify file extension
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        if(!array_key_exists($ext, $allowed)) die("Error: Please select a valid file format.");
    
        // Verify file size - 5MB maximum
        $maxsize = 5 * 1024 * 1024;
        if($filesize > $maxsize) die("Error: File size is larger than the allowed limit.");
    
        // Verify MYME type of the file
        if(in_array($filetype, $allowed)){
            // Check whether file exists before uploading it
            if(file_exists("/tmp/upload/" . $filename)){
                echo $filename . " is already exists.";
            } else{
                move_uploaded_file($_FILES["photo"]["tmp_name"], "upload/" . $filename);
                echo "Your file was uploaded successfully.";
            } 
        } else{
            echo "Error: There was a problem uploading your file. Please try again."; 
        }
        // 1. display name, size, path and extenssion
        // name:
        echo "<br>Name: " . $filename ;
        echo "<br>Size: " . $filesize;
        echo "<br>Extenssion: " . $ext . "<br>";
        echo "<br>tmpname: " . $_FILES["photo"]["tmp_name"];
    } else{
        echo "Error: " . $_FILES["photo"]["error"];
    }
}
/////////////
/// file 2///
/////////////
if($_SERVER["REQUEST_METHOD"] == "POST"){
    // Check if file was uploaded without errors
    if(isset($_FILES["photo2"]) && $_FILES["photo2"]["error"] == 0){
        $allowed = array("jpg" => "image/jpg", "jpeg" => "image/jpeg", "gif" => "image/gif", "png" => "image/png");
        $filename = $_FILES["photo2"]["name"];
        $filetype = $_FILES["photo2"]["type"];
        $filesize = $_FILES["photo2"]["size"];
    
        // Verify file extension
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        if(!array_key_exists($ext, $allowed)) die("Error: Please select a valid file format.");
    
        // Verify file size - 5MB maximum
        $maxsize = 5 * 1024 * 1024;
        if($filesize > $maxsize) die("Error: File size is larger than the allowed limit.");
    
        // Verify MYME type of the file
        if(in_array($filetype, $allowed)){
            // Check whether file exists before uploading it
            if(file_exists("/tmp/upload/" . $filename)){
                echo $filename . " is already exists.";
            } else{
                move_uploaded_file($_FILES["photo2"]["tmp_name"], "upload/" . $filename);
                echo "Your file was uploaded successfully.";
            } 
        } else{
            echo "Error: There was a problem uploading your file. Please try again."; 
        }
        // 1. display name, size, path and extenssion
        // name:
        echo "<br>Name: " . $filename ;
        echo "<br>Size: " . $filesize;
        echo "<br>Extenssion: " . $ext . "<br>";
        echo "<br>tmpname: " . $_FILES["photo2"]["tmp_name"];
    } else{
        echo "Error: " . $_FILES["photo2"]["error"];
    }
}

?>
