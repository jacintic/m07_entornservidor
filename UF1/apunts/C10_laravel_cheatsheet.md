# Commands:  
## In DB mariadb:  
```
$ su -l
# mariadb
# create database highcode;
# use highcode;
# create user admin@localhost IDENTIFIED BY 'adminhc';
# grant all privileges on highcode.* to admin@localhost;
```
## In Laravel project folder:  
```
composer install
(copy .env)
php artisan key:generate
php artisan migrate:install
php artisan migrate
php artisan db:seed
php artisan serve
```
### Laravel create table:  
```
php artisan make:migration create_tutorials_table
```
### Laravel create model:
```
php artisan make:model Tutorial
```
### Laravel create seeder:  
```
php artisan make:seeder tutorials
```

### Laravel example of fields for table (migration):  
```
// install
 public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }
// rollback
public function down()
{
    Schema::dropIfExists('users');
}
```
  
## Model:  
```
<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tutorial extends Model
{   
    // filter for HOME PAGE (cards)
    public function scopeTech($query,$tech) {
        return $query->where('tech','=',$tech)->get();
    }
    // filter for sidebar form
    public function scopeMultiple($query,$filters) {

        $queryCollection = [];
        foreach ($filters as $key => $value) {
           if ($value != null) {
               // add query to the query collection
            array_push($queryCollection,[$key,'=',$value]);
           }
        }
        // execute the query and return it
        return $query->where($queryCollection)->get();
    }
   //filter for searchbar
    public function scopeSearch($query,$title,$tipo) {
        return $query->where($tipo,'like',"%$title%")->get();
    }
    /**
     * Insert tutorial on DB
     */
    public function addTutorial($r) {
        $tutorial = new Tutorial;
        $tutorial->tech = $r["tech"];
        $tutorial->title = $r["title"];
        $tutorial->diff = $r["diff"];
        $tutorial->desc = $r["desc"];
        $tutorial->body = $r["body"];
        $tutorial->save();
    }
}
```
## Controller  
```
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tutorial;

class TutorialController extends Controller
{
    public $tutorialModel;

    public function __construct()
    {
        $this->tutorialModel= new Tutorial();
    }
    // filter for home page
    public function showTutorials($tech) {
        if ($tech == 'all') {
            return view('listTutorials')->with(['tutoriales'=>$this->tutorialModel->get()]);
        }
        return view('listTutorials')->with(['tutoriales'=>$this->tutorialModel->tech($tech)]);
    }
    // tutorial form
    public function filterForm(Request $request) {
        $tech = $request->input('tech');
        $diff = $request->input('diff');
        $duration = $request->input('duration');
        // get all tutorials
        if ($tech == 'all' && $diff == 'all' && $duration == 'all') {
            return view('listTutorials')->with(['tutoriales'=>$this->tutorialModel->get()]);
        }
        // filter by all fields
        $filters = [
            "tech" => $tech,
            "diff" => $diff,
            "duration" => $duration,
        ];
        return view('listTutorials')->with(['tutoriales'=>$this->tutorialModel->multiple($filters)]);
    }

   // Filter search
     // Filter search
     public function filterSearch(Request $request) {
        $title = $request->get('buscar');
        $tipo = $request->get('tipo');
        $tutoriales = Tutorial::search($title,$tipo);
        return view('listTutorials',compact('tutoriales'));
    }

    //Insert tutorial
    public function addTutorials(Request $datosFormulario) {        
         $datos = [
             "tech" => $datosFormulario->get('tech'),
             "title" => $datosFormulario->get('title'),
             "diff" => $datosFormulario->get('diff'),
             "desc" => $datosFormulario->get('desc'),
             "body" => $datosFormulario->get('body'),
         ];
         $this->tutorialModel->addTutorial($datos);
         return view('insertTutorial');
     }
}

```
## Routes  
```
<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Tutorial;
use App\Http\Controllers\TutorialController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('homeCards');
});

Route::get('/tutorials/{technologySelected?}', [Tutorial::class,'showTutorial']);


/* route for include, test */
Route::get('/include', function() {
    return view('include');
});

//----LOGIN/SIGN UP FORMS----//
// Sign up
Route::get('/signup', function() {
    return view('signUp');
});

// Login
Route::get('/login', function() {
    return view('logIn');
});

//----INSERTAR TUTORIAL----//
// insert form
Route::get('/insertForm', function() {
    return view('createTutorial');
});
// tutorial form post route
Route::post('/insertTutorial', [TutorialController::class,'addTutorials']);

//----FILTROS----//
// post form filters
Route::post('filter', [TutorialController::class,'filterForm']);
// SearchBar filter
Route::get('search', [TutorialController::class,'filterSearch']);
// fitler for home cards (link to tech)
Route::get('/{lang?}', [TutorialController::class,'showTutorials']);
```
## View:  
```
@extends('layouts.filter')

@section('content')

 

<form method="get" action="/search" class="form-inline my-2 my-lg-0">
    <select name="tipo" class="form-control" id="exampleFormControlSelect1">
        <option value="title">Titulo</option>
        <option value="author">Autor</option>
        <option value="tech">Tech</option>
    </select>
    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" name="buscar">
    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
</form>
@forelse ($tutoriales as $tutorial)
<div class="row border border-primary rounded m-3 p-2">
        <div class="col-2">
            <img src="img/585e4bf3cb11b227491c339a.png" class="img-fluid" alt="Imagen">
            <p class="text-center">{{$tutorial->author}}</p>
        </div>
        <div class="col-10">
            <div class="row">
                <h2>{{$tutorial->title}}</h2>
            </div>
            <div class="row">
                <h2>Difficulty: {{$tutorial->diff}}</h2>
            </div>
            <div class="row">
                <h2>Duration: {{$tutorial->duration}}</h2>
            </div>
            <div class="row">
                <button type="button" class="col-1 btn btn-primary btn-sm ml-3 p-0">{{$tutorial->tech}}</button>
            </div>
            <div class="row">
                <p>{{$tutorial->created_at}}</p>
            </div>
            <div class="row">
                <div class="col-11">
                    <p>{!!$tutorial->desc!!}
                    </p>
                </div>
                <div class="col-1 mt-5">  
                    <img src="img/follow.png" class="row rounded float-right" width="50px" alt="Seguir">
                </div>
            </div>
        </div>
    </div>
    @empty
        <h2>Oops, no tutorials here yet!</h2>
    @endforelse
@endsection

```
## Mariadb permissions/user  
![mariadb commands](mariadb.jpg)  
  
## Seeder:  
```
<?php

//namespace Database\Seeders; //<=== esto da error si lo descomentas

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(TutorialSeeder::class);
    }
}        
    class TutorialSeeder extends Seeder
    {
        public function run(){
            DB::table('tutorials')->insert([
                    [
                        'tech' => 'PHP',
                        'author' => 'Pakirrin Johnson',
                        'title' => 'Hashing passwords',
                        'diff' => 'high',
                        'duration' => 'short',
                        'desc' => 'This is a short description',
                        'body' => 'This is the body of the tutorial',
                        'created_at' => date("Y-m-d H:i:s")
                    ],
                    [
                        'tech' => 'Laravel',
                        'author' => 'Pakirringo Star',
                        'title' => 'How to use the blade engine',
                        'diff' => 'medium',
                        'duration' => 'medium',
                        'desc' => 'How to seed a database.',
                        'body' => 'This is danger test territory <h1>COOL</h1>',
                        'created_at' => date("Y-m-d H:i:s")
                    ],
                    [
                        'tech' => 'Javascript',
                        'author' => 'Manolo Perez',
                        'title' => 'Loops',
                        'diff' => 'easy',
                        'duration' => 'long',
                        'desc' => 'How to create an infinite loop in javascript using for loops.',
                        'body' => 'Just us for within for within for, it works every time. <br>More test stuff <h3>hello!</h3>.',
                        'created_at' => date("Y-m-d H:i:s")
                    ],
                    [
                        'tech' => 'Vue',
                        'author' => 'Shaun Than',
                        'title' => 'Events in Vue',
                        'diff' => 'medium',
                        'duration' => 'medium',
                        'desc' => 'Creating a todo app.',
                        'body' => 'Lorem ipsum dolor amet larari larara tal qual pascual. Torrijas con leche azucar pan y aceite.',
                        'created_at' => date("Y-m-d H:i:s")
                    ],
                    [
                        'tech' => 'Vue',
                        'author' => 'Shaun Than',
                        'title' => 'Events in Vue',
                        'diff' => 'easy',
                        'duration' => 'short',
                        'desc' => 'Creating a todo app.',
                        'body' => 'Lorem ipsum dolor amet larari larara tal qual pascual. Torrijas con leche azucar pan y aceite.',
                        'created_at' => date("Y-m-d H:i:s")
                    ],
                    [
                        'tech' => 'Vue',
                        'author' => 'Shaun Than',
                        'title' => 'Events in Vue',
                        'diff' => 'medium',
                        'duration' => 'short',
                        'desc' => 'Creating a todo app.',
                        'body' => 'Lorem ipsum dolor amet larari larara tal qual pascual. Torrijas con leche azucar pan y aceite.',
                        'created_at' => date("Y-m-d H:i:s")
                    ]
                    ,
                    [
                        'tech' => 'Vue',
                        'author' => 'Shaun Than',
                        'title' => 'Events in Vue',
                        'diff' => 'high',
                        'duration' => 'short',
                        'desc' => 'Creating a todo app.',
                        'body' => 'Lorem ipsum dolor amet larari larara tal qual pascual. Torrijas con leche azucar pan y aceite.',
                        'created_at' => date("Y-m-d H:i:s")
                    ]
                ]);  
        }
    }

```