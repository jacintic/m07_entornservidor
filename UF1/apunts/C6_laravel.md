# M07 Laravel  
## Controller  
Llamadas a la view tambien en Controller.  
Creating a controller:  
```
artisan controller:make namecontroller
```
  
### Controller's actions:  
* add
* remove
* login
  
### A Controller is:  
A Class.  
  
### A controller has:  
Methods.  
  
### Ejemplos de controller:  
* ventasController
* productosController
* ventasController
	* addProduct
	* removeProduct
	* etc.
  
### Mas ejemplos:  
```
/user/view/7
// I want to view  the profile of user id = 7
```
  
## Ejemplo de Route  
Route::get("user/view/{id}");
[ejemplo de route calling controller](https://laravel.com/docs/8.x/controllers#defining-controllers);
```
Route::get('user/{id}', [UserController::class, 'show']);
```
  

  

