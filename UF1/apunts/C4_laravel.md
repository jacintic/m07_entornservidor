# Laravel.  
  
### VSCode puede instalar un plugin de Laravel.  
Puedes acceder a consola y activar servicios via comandos en la terminal en vscode.  
  
### MVC  
Modelo abstracto de como estructurar una aplicación (pro ejemplo web).  
* Model (capa de datos): se vincula con las BBDD, información pura.
* Vista (capa de presentación): lo que se ve 
* Controlador (capa de aplicación, negocio): se encarga de ordenar y dirigir las otras dos.
  
### Composer.  
Gestor de paquetes y funcionalidades pre programadas. 
* nombre
* version
* dependencias
* url repositorio
  
#### Routing.  
Traducir url visitada a una vista que el controlador ha de hestionar. Diferencia entre `GET` y `POST`.  
```
// GET
Route::get('/', function () {
	return view('welcome'); 
})->name('home');

// POST
Route::post('/result', function () {
	'uses' => 'UserController@postSignIn',
	'as' =>	'signin' 
});
```
### Access school:  
`192.168.122.1`  
`setenforce 0` => disable selinux

