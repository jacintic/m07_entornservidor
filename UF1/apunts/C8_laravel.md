# M07 Laravel  
## VMC  
  
* URL/Form => Routes  
* Routes(param) => Controller(param) ====> Controller can get (params) without Route's help¿?¿  
* 
  
## Controllers  
  
### Naming  
* in english
* singular
* name of the object, example => ProductController => show product, edit product, etc...
  
### What it do?  
Processes information and returns a view with the data the view needs to display all the info.  
  
### Atomicity  
Products, users, tickets, offers, etc.
  
### Controlling  
If you're not logged in, I redirect you to not logged in view.    
  
### Includes  
`use /app/myClass`      
  
  

## View    
  
### In views, never make calculations  
  
### Iterate through object's collections.  
```
Foreach
```
  
### Discount  
* because you are registered and logged in  
  
  
  
## Model  
  
### Ex.  
`select products where price < 15;`
  
### Ex.2  
Product (class) => products (table)  
this is default behavior.  
 

## Check the "blade" documentation  
## Forbidden => php in Laravel  
## Forbidden2 => html inside variables  
  
  
## Check Model  
  
### Migrate. To deal with tables inside Laravel.  
Artissan migrate => create DB according to Controllers and views.  
  

