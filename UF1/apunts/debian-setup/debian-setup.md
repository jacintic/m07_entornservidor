# Debian Config  
  
## Keyboard language conf  
  
```
sudo apt-get install keyboard-configuration console-setup
```
  
## Install `man`  
  
```
sudo apt-get install man
```
  
## httpd root folder:  
  
```
/opt/bitnami/apache2/htdocs
```
  
## browser access:  
  
```
http://192.168.1.118
```
  
## SSH conf:  
  
* SSH enable: https://docs.bitnami.com/virtual-machine/faq/get-started/enable-ssh/
* ssh config: https://docs.bitnami.com/aws/faq/get-started/connect-ssh/
  
## SSH with PUTTY:  

In server type following orders:  
```
sudo rm -f /etc/ssh/sshd_not_to_be_run
sudo systemctl enable ssh
sudo systemctl start ssh
```
  
## Putty config:  
* IP: 192.168.1.118 
* SSH (default option)
* usr: bitnami
* pw: bitnami
  
## Filezilla:  
It will work as long as SSH is enabled and FTP  
  
## SSH from fedora VM to bitnami VM:  
`ssh bitnami@192.168.1.118`  
  
## SFTP from Fedora file manager:  
* Ctrl + L (show address bar in file manager, file manager mut be open)
* addr: sftp://192.168.1.118  
  
