# M07 Laravel  
  
## .gitignore  
  
### .env => APP_KEY  
```
php artisan key:generate

dbname => name of the db


```
  
### composer.json  
Composer es un gestor de paquetes. Mediante el composer.json se definen los paquetes necesarios para la aplicación (dependencias).  
  
Paquetes:  
* validacion de formularios contra BBDD
* live refresh
  
#### require  
Para la aplicación.  

#### require dev  
Para el desarollo.
  
### Composer  
=> browse packages  
Te dice los requisitos de cada paquete (dependencias).  
  
#### Install  
`compose install` => vendor => te instala un paquete (y sus dependencias). En vendor estan los paquetes nuevos, pero con compose install te ahorras todo, haces compose install y te instala todos los paquetes.  
  
#### composer packages:  
2.5.0  
2 => cambios mayores  
5 => cambios notables actualizaciones de seguridad  
0 => cambios menores => pequeños bugs normalmente ignorables  
  
`(nada)` => este paquete exacto  
`~` la version mas reciente del numero 3 (2.5.3 => el 3)  
`^` el primer numero fijo, los otros dos el mas nuevo  
  
### npm  
Igual que composer pero de creadores diferentes. Misma estructura en el package.json en vez del composer.json.  
Se llaman `dev dependencies`.  
`scripts` => npm run dev. Ejecuta del package la rama dev.  
  
#### ALERTA!  
Utilizar `npm`o `composer` pero no los dos por que te estas complicando la vida y vas a aencontrar errores.  

