# M07 Entron Servidor  
  
### Pw autoinscripció: 2020.  
  
## Fòrums: Notificacions (Nomes del profe). Fòrum de dubtes (dubtes alumnes)  
  
## Videoconferencies (per clases a distancia)  
  
## Mi grupo de proyectos: Grup ABP (Servidors).  
  
## Wiki: presentaciones, tutoriales, actividades, enlaces (editables).  
  
## UF1/2 creación pagina web. Es posible encontrarse con la necesidad de introducir contenido asincrono de back end.  
  
## UF3 (BBDD). Como conectar back end a BBDD. Contar con BBDD real.  
  
## UF4 Conexiones asincronas.  
  
## Evaluación por competencias. Lista de competencias, (No adquirida, Basica, Pro), para adquirir la Pro, tienes que enseñarselo a los compañeros.  
  
### Ejemplo:  
* C1 - 1
* C2 - 1 => 5 (Basico)
* C3 - 1
  
* C1 - 0
* C2 - 2 => 4 (No aprovado, no ha adquirido todas las competencias)
* C3 - 2  
  
## Competencias UF1  
  
* Funciones ordenadas. 
* Variables por referencia y por valor (local global).
* Auth (cookies, sesiones, estado)
* IDE, debug, testing, comentarios, control de versiones, documentación

### Git informara de quien ha hecho que, repartir las tareas para que todo el mundo haga un poco de todo.  
  
## Idea, practica de login para el proyecto. (Roles, Admin, Profe, Usuario).  
  
## API Gestiona la comunicación cliente servidor, en un protocolo standard que suele ser JSON.  
  
## Async: trabajan en segundo plano, enviando y reciviendo peticiones al servidor.  
  
### Ejemplo de comunicación asyncrona, sugerencia de busquedas, autocomplete.  
  
## POST y GET. Por GET, los valores apareceran en la URL. En POST no.  
  
### GET, ejemplo => ?info=value&name=value&...  
  
## cookies, guardar y leer información en cookies. Usuario tiene una cookie con información de haver visitado frigorificos en amazon. Yo le enseño un popup de "Comprate un frigorifico".  
  
## Framework. Conjunto de herrramientas, tecnicas, librerias. Caracteristicas sintacticas. Con sus REGLAS establecidas, tienes que aprenderlas.  
  
### Frameworks implica un tiempo de indagación para aprender la sintaxis, a partir del cual, la productividad incrementara, pues esas herramientas facilitan la interacción.  
  
### Se tocaran frameworks en esta asignatura.  
  
## Formularios, mecanismos de transferencia de información.  
  
## Pagina Web vs Aplicación Web.  
  
### Pagina Web. Lenguaje de marcas ejecutado por el navegador. Conjunto de información HTML interpretado por el navegador.  
  
### Aplicación Web. La que se ejecuta en un servidor remoto, que ejecuta un proceso y me devuelve una Pagina Web.  
  
#### Una serie de clientes que realizan una request a un servidor.  
  
## Modelo de tres capas.  
  
* Capa de Presentación. Lo visual
* Capa de Datos. BBDD, peticiones y resultados a BBDD
* Capa de Negocio. Entre medio de las otras dos, se encarga de mediar entre las dos capas.
  
### Negocio => por ejemplo API sirviendo el resultado de consultas SQL servido en JSON a presentación.  
  
* Capa de Presentación. HTML
* Capa de Datos. MySQL
* Capa de Negocio. PHP, Apache 
  
##### ASIX es recomendable para back end.  
  
## Servidor web y de aplicaciones  
  
### Deveres LAMP, + FTP + Docker? + Google Cloud/AWS?  
  
```
```

