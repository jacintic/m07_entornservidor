# M07 Servidors  
## Routes => rutas, url,y redirecciones  
## Views => paginas  
`name.blade.php`  
Por que `.blade`? => Sistema de plantillas de Laravel.  
El `blade` es un sistema de plantillas de Laravel que permite añadir funcionalidades.  
El blade hace que pase nuestro codigo por el preprocessor de Laravel.  
La traducción de esto se guarda en `storage => frameworks => views`
  
### En Laravel:  
```
@if => <?php ?>
```
  
### Formularis i verificació de formularis.  
Laravel collective
[link laravel collective](https://laravelcollective.com/docs/5.4/html)  
Usar composer a tutipleni. 
  
## A tener en cuenta al usar composer con Laravel.  
* Compativilidad con versión actual
* Buena documentación
* Vivo (no descontinuado)
* Gente participando en los comentarios
  
## Controllers  
Se encargan de recivir variables  
  
### App => Http => Controller  
Para que sirve?  

