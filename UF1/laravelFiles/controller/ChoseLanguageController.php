<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ChoseLanguageController extends Controller
{
    public $js = [
        ["diff" => "beginner",
        "title" => "tituloJS1",
        "body" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium, 
        quas? Dolor itaque recusandae, laudantium nobis soluta, expedita nam tempora,
        tenetur eaque quis maxime adipisci in fugiat molestiae harum mollitia est."
        ],
        ["diff" => "intermeditate",
        "title" => "tituloJS2",
        "body" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium, 
        quas? Dolor itaque recusandae, laudantium nobis soluta, expedita nam tempora,
        tenetur eaque quis maxime adipisci in fugiat molestiae harum mollitia est."
        ],
        ["diff" => "high",
        "title" => "tituloJS3",
        "body" => "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium, 
        quas? Dolor itaque recusandae, laudantium nobis soluta, expedita nam tempora,
        tenetur eaque quis maxime adipisci in fugiat molestiae harum mollitia est."
        ],
    ];

    private $php = [];
    private $java = [];

    /*
    public function filter(Request $n) {
        $op = $n['option'];
        $level = $n['level'];
        
        if ($level == "all") {
            foreach($this->js as $j) {
                foreach($j as $x) {
                    echo $x . "<br>";
                }
                echo "<br>";
            }
        } else if ($level == "beginner") {
            foreach($this->js as $j) {
                if ($j["diff"] == "beginner") {
                    echo $j["title"] . "<br>";
                    echo $j["body"] . "<br>";
                }              
            }
        }
 
       // return $this->tutorials($n['option'], $n['level']);
    }
    */

    public function filter() {
        $n = $this->js;
        return view("javascriptview", compact("n"));
    }




    //------------------------
    public function tutorials($language, $level) {
        return "$language : $level";
    }

    public function tutorialList(Request $n) {
        return $this->tutorials($n['option'], $n['level']);
    }

}
