<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ChoseLanguageController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/a', function() {
    return '<h1>Hello World</h1>';
});
Route::get('/f', function() {
    return view('form');
});
Route::post('/language',[ChoseLanguageController::class, 'filter']);
// frankenstain (includes) test
Rout::get('/fr', function() {
    return view('frankenstain');
});